
```shell script


reset; cat /tmp/bash1.sh | jq -r --arg ARCH amd64 "$(cat <<-'REPORT'
  .versions[
     .versions|keys|map(
       select(contains("-")|not)|split(".")|map(tonumber)
    )|max|join(".")
  ] | 
  (.builds|map(select(.os == "linux" and .arch == $ARCH))[]|.url) as $url |
  {
    hashi_version: .version,
    hashi_url: $url,
    hashi_sig: [$url[0:$url|rindex("/")], .shasums_signature]|join("/"),
    hashi_sum: [$url[0:$url|rindex("/")], .shasums]|join("/")
  } | keys[] as $k | "\($k)=\(.[$k])"

REPORT
)"


```