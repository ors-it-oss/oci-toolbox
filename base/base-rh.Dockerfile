ARG FROM_IMAGE=registry.gitlab.com/ors-it-oss/oci-base/fedora
ARG FROM_BASE_TAG=latest
ARG FROM_SCRIPTS_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/scripts:${FROM_SCRIPTS_TAG} AS scriptsrc
FROM ${FROM_IMAGE}:${FROM_BASE_TAG}

LABEL \
	org.opencontainers.image.description="Base container for Toolboxxes"

COPY --from=scriptsrc include/python/ include/toolbox/ /
COPY --from=scriptsrc \
	include/install/usr/libexec/RUN.d/github.sh \
	include/install/usr/libexec/RUN.d/security-tools.sh \
	/usr/libexec/RUN.d/

ENV PIP_NO_CACHE_DIR=1
RUN source /usr/libexec/RUN.sh ;\
	shell_pkgs="bash-completion file findutils jq less ncdu sl tmux tree which"  ;\
	net_pkgs="openssh-clients rsync socat telnet wget"  ;\
	lib_pkgs="groff-base jq openssl ncurses unzip" ;\
	devel_pkgs="diffutils git-core make vim-minimal" ;\
	py_pkgs="python python-pip python-setuptools python-wheel" ;\
	pkg_run $py_pkgs $shell_pkgs $net_pkgs $lib_pkgs $devel_pkgs

# Think Byobu be dead
#	&&\
#	find /etc/skel/.byobu -type d | xargs chmod 755 &&\
#	find /etc/skel/.byobu -type f | xargs chmod 644

ARG APG_VERSION=latest
RUN /usr/libexec/RUN.sh apg_install

#ENV \
#	HOME=/home \
#	XDG_CACHE_HOME=/var/cache \
#	XDG_DATA_HOME=/var
#
#VOLUME /home

CMD ["/bin/bash", "-l"]
